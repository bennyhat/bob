﻿param(
  [parameter(Position=0)][string] $command,
  [parameter(Position=1)][string] $target,
  [parameter(Position=2)][string] $location,
  [string] $arguments,
  [string] $version,  
  [string] $group,
  [string] $environment="local",
  [string] $server="default",
  [alias("v")][switch] $verbosity = $false,
  [alias("q")][switch] $quick = $false,
  [alias("d")][switch] $debugging = $false,
  [string[]]$additional=@('')
)

# extra args that we couldn't override from PowerShell
if ($PSBoundParameters['Debug']) {
	$debugging = $true
}
if ($PSBoundParameters['Verbose']) {
	$verbosity = $true
}

# bob
# Adapted from chocolatey powershell scripts - https://chocolatey.org/

## Set the culture to invariant
$tCurrentThread = [System.Threading.Thread]::CurrentThread;
$ciCulture = [System.Globalization.CultureInfo]::InvariantCulture;
$tCurrentThread.CurrentCulture = $ciCulture;
$tCurrentThread.CurrentUICulture = $ciCulture;

## Some global variables that are used for pathing, etc.
$global:sBobVersion = "0.0.1"
$global:sBobPath = Split-Path -parent $MyInvocation.MyCommand.Definition

$PSModuleAutoLoadingPreference = "All";

$global:sColorDebug = "Magenta"
$global:sColorError = "Red"
$global:sColorSuccess = "Green"
$global:sColorNormal = "White"

## Load in extra modules from the helpers directory
$sInstallModule = Join-Path $sBobPath (Join-Path 'helpers' 'bobInstaller.psm1')
Import-Module $sInstallModule 3>$null

## Load in functions from the core functions directory
Resolve-Path $(Join-Path $sBobPath (Join-Path "functions" "*.ps1")) |
    ? { -not ($_.ProviderPath.Contains(".Tests.")) } |
    % { . $_.ProviderPath }
	
$global:bDebugSet = $debugging
$global:bQuickSet = $quick
$global:bVerbositySet = $verbosity

$global:sDefaultBuildTarget = "dsw-com"
$global:sDefaultBuildLocation = $(Join-Path $(Get-EnvironmentVariable("PATH_HOME")) "code")
$global:sDefaultDeployTarget = "dsw-com-env"
$global:sDefaultDeployGroup = "com.dsw.atg.env"
$global:sDefaultRunTarget = "jboss"

# Main command switch start
Write-Debug "command = '$command'|target='$target'|arguments='$arguments'|version='$version'|location='$location'`
|environment='$environment'|server='$server'|verbosity='$verbosity'|debugging='$debugging'|quick='$quick'|additional='$additional'`
|PowerShellVersion=$($host.version)|OSVersion=$([System.Environment]::OSVersion.Version.ToString())"

try {
	switch -wildcard ($command) {
	  "build"	{ Bob-Build $target $arguments $version $location $environment $server }
	  "clean"	{ Bob-Clean $target $arguments $version $location $environment $server }
	  "deploy"	{ Bob-Deploy $target $group $arguments $version $location $environment $server }
	  "run"		{ Bob-Run $target $arguments $version $location $environment $server }
	  "help"	{ Bob-Help $target $arguments $version $location $environment $server }
	  default	{ Write-Host "Please run bob /? or bob help - bob v$global:sBobVersion";}
	}
}
catch {
	$bBobErrored = $true
	Write-Host "$($_.Exception.Message)" -BackgroundColor $sColorError -ForegroundColor White ;
}
finally {
	# nothing for now
}

if ($bBobErrored) {
  Write-Debug "Exiting with non-zero exit code."
  exit 1
}
exit 0
