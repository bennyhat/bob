﻿function Bob-PrepareTarget {
param(
  [string] $target,
  [string] $version,
  [string] $location
)
	# some preparation for XPath
	$nsDefault = @{dns = 'http://maven.apache.org/POM/4.0.0'} # seriously...MS requires a namespace, even if it's the default
	
	# do a tree search for poms that have an artifact matching the target
	$sPomPath = Join-Path $location "pom.xml"
	$sPomObjects = Get-ChildItem "$sPomPath" -recurse -erroraction SilentlyContinue
	Foreach ($fPom in $sPomObjects) {
		$sArtifactId = $(Select-Xml -xpath "/dns:project/dns:artifactId" -namespace $nsDefault -path $fPom.FullName | Select-Object -ExpandProperty Node).InnerText
		
		# if the artifact doesn't match the target then just pass by it
		if (-not ($sArtifactId -eq $target)) { continue }
		
		# we found it so we can get path and exit
		$sTargetPath = Split-Path -parent $fPom.FullName
		break # don't run this loop longer than necessary
	}
	# check if we even found the target
	if ([string]::IsNullOrEmpty($sTargetPath)) { throw "[ERROR] Target $target was not found." }	
	
	# check the version and checkout if necessary (no version set will leave it alone)
	Bob-CheckoutTargetVersion $version $sTargetPath
	
	# check the POM to actually get the version of the artifact (the branch version is not reliable for env detection)
	$sArtifactVersion = Bob-GetTargetVersion $fPom.FullName
	$iArtifactVersion = $sArtifactVersion.Split(".")[0] -as [int]
	Bob-PrepareEnvironment $iArtifactVersion
	
	# one nice thing PS does (and it's one of the few) -- supports tuple returns
	return $sTargetPath, $sArtifactVersion
}
