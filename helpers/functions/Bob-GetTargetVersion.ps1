﻿function Bob-GetTargetVersion {
param(
  [string] $targetPath
)
	$nsDefault = @{dns = 'http://maven.apache.org/POM/4.0.0'} # seriously...MS requires a namespace, even if it's the default
	$sArtifactVersion = $(Select-Xml -xpath "/dns:project/dns:version" -namespace $nsDefault -path $targetPath | Select-Object -ExpandProperty Node).InnerText
	if ([string]::IsNullOrEmpty($sArtifactVersion)) {
		$sArtifactVersion = $(Select-Xml -xpath "/dns:project/dns:parent/dns:version" -namespace $nsDefault -path $targetPath | Select-Object -ExpandProperty Node).InnerText
	}
	
	return $sArtifactVersion
}
