﻿function Write-Debug {
param(
  [Parameter(Position=0,Mandatory=$false,ValueFromPipeline=$true, ValueFromRemainingArguments=$true)][object] $Object,
  [Parameter()][switch] $NoNewLine,
  [Parameter(Mandatory=$false)][ConsoleColor] $ForegroundColor,
  [Parameter(Mandatory=$false)][ConsoleColor] $BackgroundColor,
  [Parameter(Mandatory=$false)][Object] $Separator
)
  if (!$global:bDebugSet) { return }
  $oc = Get-Command 'Write-Host' -Module 'Microsoft.PowerShell.Utility'
  & $oc -ForegroundColor $global:sColorDebug @PSBoundParameters
}
