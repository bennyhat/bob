function Set-EnvironmentVariable() {
param(
[string] $name, 
[string] $value,
[System.EnvironmentVariableTarget] $scope = [System.EnvironmentVariableTarget]::User
)
    [Environment]::SetEnvironmentVariable($name, $value, $scope)
}
