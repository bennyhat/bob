﻿function Write-Error {
param(
  [Parameter(Position=0,Mandatory=$true,ValueFromPipeline=$true)][string] $Message='',
  [Parameter(Mandatory=$false)][System.Management.Automation.ErrorCategory] $Category,
  [Parameter(Mandatory=$false)][string] $ErrorId,
  [Parameter(Mandatory=$false)][object] $TargetObject,
  [Parameter(Mandatory=$false)][string] $CategoryActivity,
  [Parameter(Mandatory=$false)][string] $CategoryReason,
  [Parameter(Mandatory=$false)][string] $CategoryTargetName,
  [Parameter(Mandatory=$false)][string] $CategoryTargetType,
  [Parameter(Mandatory=$false)][string] $RecommendedAction,
  [Parameter(Mandatory=$false)][ConsoleColor] $ForegroundColor = $global:sColorError,
  [Parameter(Mandatory=$false)][ConsoleColor] $BackgroundColor = "White"
)

  $oc = Get-Command 'Write-Error' -Module 'Microsoft.PowerShell.Utility' 
  & $oc -BackgroundColor $global:sColorError -ForegroundColor White @PSBoundParameters
}