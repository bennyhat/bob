﻿function Bob-CheckoutTargetVersion {
param(
  [string] $version,
  [string] $targetPath
)
	if (-not ([string]::IsNullOrEmpty($version))) {
		$sGitBinary = Get-Command "git" | Select-Object -ExpandProperty Definition
		Bob-StartProcess $sGitBinary "checkout $version" $targetPath
	}
}
