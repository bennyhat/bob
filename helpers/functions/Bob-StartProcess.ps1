﻿function Bob-StartProcess {
param(
  [string] $file,
  [string] $arguments = $args,
  [string] $location
)
	if ([string]::IsNullOrEmpty($location)) { $location = Get-Location }

	Write-Success "Running $file $arguments in $location."
	$psiBobProcess = new-object System.Diagnostics.ProcessStartInfo $file;
	$psiBobProcess.Arguments = $arguments;
	$psiBobProcess.UseShellExecute = $false;
	$psiBobProcess.CreateNoWindow = !$global:bVerbositySet
	$psiBobProcess.WorkingDirectory = $location;
	if ($global:bVerbositySet) {
		$psiBobProcess.WindowStyle = "Hidden"
	}
	
	$pBobProcess = New-Object System.Diagnostics.Process
	$pBobProcess.StartInfo = $psiBobProcess
	
	$pBobProcess.Start() | Out-Null
    $pBobProcess.WaitForExit() | Out-Null
	
	if ($pBobProcess.ExitCode -ne 0) {
		throw "[ERROR] Running $file with $arguments was not successful."
	}
}
