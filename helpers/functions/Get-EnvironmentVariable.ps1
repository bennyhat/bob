function Get-EnvironmentVariable() {
param(
[string] $name, 
[System.EnvironmentVariableTarget] $scope = [System.EnvironmentVariableTarget]::User
)
	# first check in shell environment
	$sShellValue = [Environment]::GetEnvironmentVariable($name, [System.EnvironmentVariableTarget]::Process)
	if (-not ([string]::IsNullOrEmpty($sShellValue))) { return $sShellValue }
	
	# then in scoped environment
    return [Environment]::GetEnvironmentVariable($name, $scope)
}
