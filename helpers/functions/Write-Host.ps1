﻿function Write-Host {
param(
  [Parameter(Position=0,Mandatory=$false,ValueFromPipeline=$true, ValueFromRemainingArguments=$true)][object] $Object,
  [Parameter()][switch] $NoNewLine,
  [Parameter(Mandatory=$false)][ConsoleColor] $ForegroundColor,
  [Parameter(Mandatory=$false)][ConsoleColor] $BackgroundColor,
  [Parameter(Mandatory=$false)][Object] $Separator
)

  $oc = Get-Command 'Write-Host' -Module 'Microsoft.PowerShell.Utility'
  & $oc @PSBoundParameters
}
