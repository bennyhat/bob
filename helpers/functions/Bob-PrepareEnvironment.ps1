﻿function Bob-PrepareEnvironment {
param(
  [int] $version
)
	# set environment variables based off of the version
	if ($version -lt 10) {
		$env:JAVA_HOME = Get-EnvironmentVariable "PATH_JAVA_6"
		$env:ATG_INSTALL_HOME = Get-EnvironmentVariable "PATH_ATG_9"
		$env:JBOSS_HOME = Get-EnvironmentVariable "PATH_JBOSS_4"
		$env:JBOSS_AS_HOME = Join-Path $env:JBOSS_HOME "jboss-as"
		$sDeployRoot = Join-Path "$env:JBOSS_AS_HOME\server" $global:sDefaultDeployServer
		$env:JBOSS_DEPLOY = Join-Path $sDeployRoot $(Join-Path "deploy" $global:sDefaultDeployApplication)
	}
	else {
		$env:JAVA_HOME = Get-EnvironmentVariable "PATH_JAVA_7"
		$env:ATG_INSTALL_HOME = Get-EnvironmentVariable "PATH_ATG_11"
		$env:JBOSS_HOME = Get-EnvironmentVariable "PATH_JBOSS_6"
		$env:JBOSS_AS_HOME = $env:JBOSS_HOME
		$env:JBOSS_DEPLOY = Join-Path "$env:JBOSS_AS_HOME\standalone\deployments" $global:sDefaultDeployServer
	}
	$env:JAVA_VM = Join-Path $env:JAVA_HOME $(Join-Path "bin" "java")
	$env:ATG_HOME = Join-Path $env:ATG_INSTALL_HOME "home"
	$env:ATG_ROOT = Split-Path -parent $env:ATG_INSTALL_HOME
}
