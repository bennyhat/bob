﻿$sHelpersPath = (Split-Path -parent $MyInvocation.MyCommand.Definition);

# grab functions from files
Resolve-Path $sHelpersPath\functions\*.ps1 |
    ? { -not ($_.ProviderPath.Contains(".Tests.")) } |
    % { . $_.ProviderPath }
 
Export-ModuleMember -Function `
	Get-EnvironmentVariable,`
	Set-EnvironmentVariable,`
	Remove-EnvironmentVariable,`
	Write-Folder,`
	Remove-Folder,`
	Bob-StartProcess,`
	Bob-PrepareTarget,`
	Bob-PrepareEnvironment,`
	Bob-GetTargetVersion,`
	Bob-CheckoutTargetVersion,`
	Write-Error,`
	Write-Host,`
	Write-Success,`
	Write-Debug
