function Bob-Help {
@"
Version: `'$global:sBobVersion'`
Install Directory: `'$global:sBobPath`'

== Commands ==
Full reference coming soon:

 * build - bob build something
 * clean - bob clean something
 * deploy - bob deploy something (will both clean and build before deploying if force flag is set)
 * run - bob run something

More advanced commands and switches listed on the command reference

 Examples:
  * bob build dsw-com-commerce -version 3.7.0-SNAPSHOT
  * bob deploy dsw-com-commerce-app -destination C:\dev\jboss\deploy -version 11.1.0-SNAPSHOT -f
  * bob run jboss-4 -version 11.1.0-SNAPSHOT

"@ | Write-Host
}
