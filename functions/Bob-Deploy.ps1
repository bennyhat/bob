function Bob-Deploy {
param(
  [string] $target,
  [string] $group,
  [string] $arguments,
  [string] $version,
  [string] $location,
  [string] $environment,
  [string] $server
)
	# if no version provided, get it from the default target from the default location
	if ([string]::IsNullOrEmpty($target)) { $target = $global:sDefaultDeployTarget }
	if ([string]::IsNullOrEmpty($group)) { $group = $global:sDefaultDeployGroup}
	if ([string]::IsNullOrEmpty($version)) {  
		$sDetectTarget = $global:sDefaultBuildTarget
		$sDetectLocation = $global:sDefaultBuildLocation
		$sTargetPath, $version = Bob-PrepareTarget $sDetectTarget $version $sDetectLocation
	}	
	
	# location is the deploy destination. needs to be after the detection to have the right environment variable setup
	if ([string]::IsNullOrEmpty($location)) { $location = Get-EnvironmentVariable "JBOSS_AS_HOME" }
	
	# finally, get the default location of our maven pom
	$sDeployPath = Join-Path $global:sBobPath $(Join-Path "maven" "deploy")
	
	# utilize maven to deploy the target
	$sMavenBinary = Get-Command "mvn" | Select-Object -ExpandProperty Definition
	$sMavenArguments = "install -Dgroup=$group -Dtarget=$target -Dversion=$version -Dlocation=$location -Denvironment=$environment -Dserver=$server"
	Bob-StartProcess $sMavenBinary $sMavenArguments $sDeployPath
	
	Write-Success "Deploy of $target successfully completed!"
}
