function Bob-Run {
param(
  [string] $target,
  [string] $group,
  [string] $arguments,
  [string] $version,
  [string] $location,
  [string] $environment,
  [string] $server
)
	# if no version provided, get it from the default target from the default location
	if ([string]::IsNullOrEmpty($target)) { $target = $global:sDefaultRunTarget }
	if ([string]::IsNullOrEmpty($version)) {
		$sDetectTarget = $global:sDefaultBuildTarget
		$sDetectLocation = $global:sDefaultBuildLocation
		$sTargetPath, $version = Bob-PrepareTarget $sDetectTarget $version $sDetectLocation
	}	
		
	# finally, get the default location of our maven pom
	$sDeployPath = Join-Path $global:sBobPath $(Join-Path "maven" "deploy")
	
	# prepare the run and select the correct binary to use
	$sRunBinary, $sRunBinaryLocation = Bob-PrepareRun $target 
	if ([string]::IsNullOrEmpty($sRunBinaryLocation)) { $location = $sRunBinaryLocation }
	
	# use the correct binary to run
	Bob-StartProcess $sRunBinary $arguments $location
	
	Write-Success "Run of $target successfully completed!"
}
