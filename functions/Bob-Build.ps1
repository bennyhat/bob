function Bob-Build {
param(
  [string] $target,
  [string] $arguments,
  [string] $version,
  [string] $location,
  [string] $environment,
  [string] $server
)
	if ([string]::IsNullOrEmpty($target)) { $target = $global:sDefaultBuildTarget }
	if ([string]::IsNullOrEmpty($location)) { $location = $global:sDefaultBuildLocation }

	# utilize maven (for now) to find the target
	$sTargetPath, $sArtifactVersion = Bob-PrepareTarget $target $version $location
	
	# utilize maven (for now) to clean the target
	$sMavenBinary = Get-Command "mvn" | Select-Object -ExpandProperty Definition
	Bob-StartProcess $sMavenBinary "install $arguments" $sTargetPath
	
	Write-Success "Build of $target successfully completed!"
}
